#Dorothy - BodyEditor
&emsp;&emsp;这是一个使用Dorothy游戏框架开发，用于配合Dorothy框架使用的物理编辑器。在这里提供二进制可执行版本的下载。详细使用说明见：[这里](http://www.luvfight.me/dorothy-body-editor/)。  
&emsp;&emsp;编辑器在初次运行后可以在Win下的`C:\Users\Username\AppData\Local\Dorothy\Body`，或是在Mac下的`/Users/Username/Library/Caches/Body`路径中找到编辑器资源的输入输出目录。  
&emsp;&emsp;生成的物理对象文件在Dorothy游戏框架中的使用方法：将以.body结尾的生成的文件复制到可访问的程序资源目录下，拷贝Lib下的oBodyEx.lua库文件，然后在代码中引用和进行加载。示例的加载代码如下：  
```lua
setfenv(Dorothy())
local oBody = require("oBodyEx")

local scene = CCScene()

local world = oWorld() -- 创建物理世界
world:setShouldContact(0,0,true) -- 设置第0组的物理对象之间会发生碰撞
world.showDebug = true -- 显示调试图形
scene:addChild(world)

local car = oBody("BodyEditor/Body/Output/car.body",world) -- 加载物理对象文件
car.data.wheel.enabled = true -- 启动物理对象中的wheel发动机
world:addChild(car)

CCDirector:run(scene)
```
&emsp;&emsp;该编辑器全部使用Dorothy框架的Lua接口提供的功能完成开发，所有的源码见`BodyEditor/Script`目录下的所有lua代码。了解Dorothy游戏框架：[这里](https://git.oschina.net/pig/Dorothy.git)。以下是部分编辑器的功能截图。  

![编辑器截图](http://git.oschina.net/pig/BodyEditor/raw/master/BodyEditor/bodyEditorUI.gif)
![编辑器截图](http://git.oschina.net/pig/BodyEditor/raw/master/BodyEditor/bodyEditorUI1.gif)
![编辑器截图](http://git.oschina.net/pig/BodyEditor/raw/master/BodyEditor/bodyEditorUI2.gif)
